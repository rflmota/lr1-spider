// Arduino MAVLink test code.

#include <NMEAGPS.h>
#include <GPSport.h>
#define MAVLINK_MAX_PAYLOAD_LEN 64

//#include <FastSerial.h>
#include <Arduino.h>
#include <mavlink.h>        // Mavlink interface

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

#include "RTClib.h"

#include <Servo.h>

#include <Streamers.h>
#include <stdint.h>

static NMEAGPS  gps;
static gps_fix  fix;

Servo drive_servo;
Servo turn_servo;


RTC_DS3231 rtc;

#define LR1_ID 1

Adafruit_BNO055 bno = Adafruit_BNO055(55);

//Initialize Timers
unsigned long heartbeatTimer_TX = millis();
unsigned long heartbeatTimer_RX = millis();
unsigned long heartbeatInterval_TX = 0.05L * 1000L;
unsigned long heartbeatInterval_RX = 2L * 1000L;

mavlink_heartbeat_t heartbeat;
uint8_t buf[MAVLINK_MAX_PACKET_LEN];
mavlink_message_t mavlink_msg;
//mavlink_timesync_t timesync;

int debug_counter = 0;

void killMotors();

static void GPSisr(uint8_t c)
{
	gps.handle(c);

} // GPSisr

void setup() {
	Serial.begin(57600);
	//while (!Serial);
	//Serial1.begin(57600);
	//while(!Serial1)
	pinMode(LED_BUILTIN, OUTPUT);
	gpsPort.attachInterrupt(GPSisr);
	gpsPort.begin(38400);

	drive_servo.attach(3);
	turn_servo.attach(2);

	drive_servo.writeMicroseconds(1500);
	turn_servo.writeMicroseconds(1500);

	///* Initialise the sensor */
	while (!bno.begin())
	{
		/* There was a problem detecting the BNO055 ... check your connections */
		//Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
		digitalWrite(LED_BUILTIN, HIGH);
	}
	digitalWrite(LED_BUILTIN, LOW);
	bno.setExtCrystalUse(true);

	
}

void loop() {

	comm_receive();

	if (gps.available(gpsPort))
	{
		fix = gps.read();
		mavlink_gps_raw_int_t gps_raw;
		if (fix.valid.altitude) {
			mavlink_msg_gps_raw_int_pack(LR1_ID, MAV_COMP_ID_GPS, &mavlink_msg, 0, GPS_FIX_TYPE_3D_FIX, fix.latitudeL(), fix.longitudeL(), fix.altitude(), 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, fix.satellites);
		}
		else {
			mavlink_msg_gps_raw_int_pack(LR1_ID, MAV_COMP_ID_GPS, &mavlink_msg, 0, fix.valid.location ? GPS_FIX_TYPE_2D_FIX : GPS_FIX_TYPE_NO_FIX, fix.latitudeL(), fix.longitudeL(), fix.altitude(), 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, fix.satellites);
		}
		uint16_t len = mavlink_msg_to_send_buffer(buf, &mavlink_msg);
		Serial.write(buf, len);

		if (fix.valid.location)
		{
			mavlink_msg_global_position_int_pack(LR1_ID, MAV_COMP_ID_GPS, &mavlink_msg, 0, fix.latitudeL(), fix.longitudeL(), fix.altitude(), 0, 0, 0, 0, fix.heading());
			len = mavlink_msg_to_send_buffer(buf, &mavlink_msg);
			Serial.write(buf, len);
		}
	}


	//kill motors if lost connection with base station (ONLY USE THIS FOR GROUND BOTS!)
	if ((millis() - heartbeatTimer_RX) > heartbeatInterval_RX)
	{
		killMotors();
	}

	if ((millis() - heartbeatTimer_TX) > heartbeatInterval_TX)
	{

		// Pack the message 
		mavlink_msg_heartbeat_pack(LR1_ID, MAV_COMP_ID_ALL, &mavlink_msg, MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC, MAV_MODE_MANUAL_ARMED /*|| MAV_MODE_FLAG_SAFETY_ARMED || MAV_MODE_FLAG_MANUAL_INPUT_ENABLED*/, 0, MAV_STATE_ACTIVE);

		// Copy the xmessage to send buffer 
		uint16_t len = mavlink_msg_to_send_buffer(buf, &mavlink_msg);
		Serial.write(buf, len);

		sensors_event_t event;
		bno.getEvent(&event);

		//imu::Quaternion quat = bno.getQuat();

		imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

		mavlink_attitude_quaternion_t attitude_quat;
		mavlink_attitude_t attitude;

		attitude.pitch = eul
		attitude.yaw = euler.z();
		attitude.roll = euler.x();
		/*attitude_quat.q2 = quat.x();
		attitude_quat.q3 = quat.y();
		attitude_quat.q4 = quat.z();*/

		mavlink_msg_attitude_pack(LR1_ID, MAV_COMP_ID_ALL, &mavlink_msg, 0, euler.x, euler.y, euler.z, 0, 0, 0);
		len = mavlink_msg_to_send_buffer(buf, &mavlink_msg);
		Serial.write(buf, len);

		heartbeatTimer_TX = millis();
	}
}

void comm_receive() {

	mavlink_message_t msg;
	mavlink_status_t status;
	mavlink_set_mode_t mode;
	mavlink_manual_control_t manual_control;
	mavlink_ping_t ping;

	while (Serial.available())
	{
		uint8_t c = Serial.read();
		// Try to get a new message
		if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status)) { 
			switch (msg.msgid)
			{
				case MAVLINK_MSG_ID_HEARTBEAT:
				{
					heartbeatTimer_RX = millis();
				}
				break;
				case MAVLINK_MSG_ID_PING:
					//mavlink_msg_ping_decode(&msg, &ping);
				case MAVLINK_MSG_ID_COMMAND_LONG:
					// EXECUTE ACTION
				break;

				case MAVLINK_MSG_ID_SET_MODE: //Get new base mode
					killMotors(); //Get bot ready for new mode
					mavlink_msg_set_mode_decode(&msg, &mode);
					//Serial1.print(F("Target System: "));
					//Serial1.print(mode.target_system);
					//Serial1.print(F("\n"));
					//Serial1.print(F("New Base Mode: "));
					/*Serial.print(mode.base_mode);*/
					heartbeat.base_mode = mode.base_mode;
					//Serial.print(F("\n"));
					//Serial.print(F("New Custom Mode: "));
					//Serial.print(mode.custom_mode);
					heartbeat.custom_mode = mode.custom_mode;
					//Serial.print(F("\n"));

					break;

				case MAVLINK_MSG_ID_MANUAL_CONTROL:
					mavlink_msg_manual_control_decode(&msg, &manual_control);
					drive_servo.writeMicroseconds(map(manual_control.x, -1000, 1000, 1000, 2000));
					turn_servo.writeMicroseconds(map(manual_control.y, -1000, 1000, 1000, 2000));
					break;
				default:
				//Do nothing
				break;
			}
		}

		// And get the next one
	}
}


void killMotors() {
	drive_servo.writeMicroseconds(1500);
	turn_servo.writeMicroseconds(1500);
}


